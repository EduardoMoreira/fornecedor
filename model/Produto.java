package model;

public class Produto {

    private String nomeProduto;
    private String descricao;
    private double valorCompra;
    private double valorVenda;
    private double quantidade;
    
    public Produto(String nomeProduto, String descricao, double valorCompra, double valorVenda, double quantidade){
        this.nomeProduto=nomeProduto;
        this.descricao=descricao;
        this.valorCompra=valorCompra;
        this.valorVenda=valorVenda;
        this.quantidade=quantidade;
    }
    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getNomeProduto() {
        return nomeProduto;
    }

    public void setNomeProduto(String nome) {
        this.nomeProduto = nomeProduto;
    }

    public double getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(double quantidade) {
        this.quantidade = quantidade;
    }

    public double getValorCompra() {
        return valorCompra;
    }

    public void setValorCompra(double valorCompra) {
        this.valorCompra = valorCompra;
    }

    public double getValorVenda() {
        return valorVenda;
    }

    public void setValorVenda(double valorVenda) {
        this.valorVenda = valorVenda;
    }
    
}
